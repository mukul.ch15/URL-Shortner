A simple url shortner using django elastic search.

Requirements

Python 2.7

Django 1.8

Elasticsearch 2.3

The project uses the pybloomfiltermmap module for the bloom filter data structure. It runs only on 
linux systems.