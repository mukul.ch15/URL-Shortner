from django.shortcuts import render, redirect
from django.http import HttpResponse
from . import forms
import base64
import random
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from pybloomfilter import BloomFilter


def home(req):
    i = 5
    if req.method == 'POST':

        

        try:
            bf = BloomFilter.open("urls.bloom")
            bf2 = BloomFilter.open("urls2.bloom")
        except Exception:
            bf = BloomFilter(100000, 0.1, "urls.bloom")
            bf2 = BloomFilter(100000, 0.1, "urls2.bloom")

        client = Elasticsearch()
        request = req.POST
        url = request["url"]

        if url not in bf:
            print "hello"
            base = base64.b64encode(url)
            base = list(base)
            temp = ""
            for val in range(0, i):
                temp = temp + base[random.randint(0, len(base))]

            temp = "http://localhost:8000/s/" + temp

            ######### Reverse checking to check for short url
            if temp in bf2:
                count = 0
                while True:
                    count += 1
                    if count == 10:
                        i += 1
                    base = base64.b64encode(url)
                    base = list(base)
                    temp = ""
                    for val in range(0, i):
                        temp = temp + base[random.randint(0, len(base))]

                    temp = "http://localhost:8000/s/" + temp
                    if temp not in bf2:
                    	break
            print url in bf
            bf.add(url)
            bf2.add(temp)
            client.index("url_shortner", "url", {"long_url": url, "short_url": temp})







        else:
            print "inside else"
            S = Search(using=client, index="url_shortner", doc_type="url").query("match", long_url=url)
            for hit in S:
                temp = hit.short_url
                break

        return render(req, "result.html", {"res": temp})
    else:
        form = forms.searchForm()
        return render(req, "test.html",{"form":form})


def search(req):
    url = "http://localhost:8000" + req.path
    client = Elasticsearch()
    S = Search(using=client, index="url_shortner", doc_type="url").query("match", short_url=url)
    for hit in S:
        temp = hit.long_url
        break
    return redirect(temp)
